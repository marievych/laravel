<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Jobs\CreateNote;
use App\Models\Note;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Queue;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\HttpFoundation\JsonResponse;

class NotesController extends Controller
{
    public function index(): JsonResponse
    {
        /*$notes = Note::take(10)->get();

        return $notes->toJson();*/
        for ($i = 0; $i < 1000000; $i++) {
            $note = new Note();
            $note->text = "Note $i";
            CreateNote::dispatch($note);
        }

        return response()->json(['message' => 'OK']);
    }

    /**
     * @param int $id
     * @return string
     * @throws ModelNotFoundException<Note>
     */
    public function view(int $id): string
    {
        $note = Cache::get("notes:$id") ?? Note::findOrFail($id);

        return $note->toJson();
    }

    public function post(Request $request): Response
    {
        $note = new Note($request->all());
        $res = CreateNote::dispatch($note);

        return response($note);
    }
}
