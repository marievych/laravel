FROM php:8.2-fpm

RUN apt update

RUN apt install -y git libzip-dev libxml2-dev

RUN pecl install redis xdebug

RUN curl -sS https://getcomposer.org/installer -o composer-setup.php \
    && php composer-setup.php --install-dir=/usr/local/bin --filename=composer

RUN docker-php-ext-install zip xml mysqli pdo pdo_mysql && docker-php-ext-enable zip xml mysqli pdo pdo_mysql redis #xdebug

COPY docker-php-ext-xdebug.ini $PHP_INI_DIR/conf.d/

USER 1000

WORKDIR /var/www/app

COPY app/ /var/www/app/

RUN composer install --no-interaction --dev